package edu.my;

import java.math.BigInteger;

/**
 * Created by Alex on 23.11.2016.
 */
public class Receiver extends User {

    // Принимающая сторона получает числа a, n от передающей стороны

    public void get_a(int a) {
        this.a = a;
    }

    public void get_n(BigInteger n) {
        this.n = n;
    }

    public void decrypt() {
        Coder coder = new Coder();
        int offset = k.mod(BigInteger.valueOf(26)).intValue();
        coder.getChangedAlphabet(26 - offset);
        coder.encrypt("Container.txt", "Decrypted.txt");
    }

}
