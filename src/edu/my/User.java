package edu.my;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Alex on 23.11.2016.
 */
public class User {

    protected BigInteger x, n, l, k;
    protected int a;

    /* Каждый из участников передачи генерирует большое число (посылающий - степень x,
    принимающий - степень y). */

    public User() {
        x = new BigInteger(256, new Random());
    }

    /* Каждый из участников передачи рассчитывает L (посылающий - L0, принимающий - Lk) */

    public void L() {
        l = BigInteger.valueOf(a).modPow(x, n);
        try (FileWriter fw = new FileWriter("Container.txt", false)) {
            fw.write(l.toString());
            fw.flush();
        } catch (IOException e) {
            System.out.println("I/O Error: " + e);
        }

    }

    /* Каждый из участников передачи рассчитывает K (посылающий - Kk на основе полученного Lk,
    принимающий - K0 на основе полученного L0). Это и есть ключ для шифрования - у адресата и
    у получателя ключи K совпадают. */

    public void K() {
        try (BufferedReader fr = new BufferedReader(new FileReader("Container.txt"))) {
            BigInteger L = new BigInteger(fr.readLine());
            k = L.modPow(x,n);
        } catch (IOException e) {
            System.out.println("I/O Error: " + e);
        }
    }


}
