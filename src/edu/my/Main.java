package edu.my;

public class Main {

    public static void main(String[] args) {

        Sender A = new Sender();                 // Агент А отправляет информацию
        Receiver B = new Receiver();             // Агент B получает информацию
        B.get_a(A.generate_a());                 // Агент B получает от агента A
        B.get_n(A.generate_n());                 // числа a, n
        A.L();                              // B вычисляет ключ K0 на основе ключа L0, полученного от A
        B.K();
        B.L();                              // A вычисляет ключ Kk на основе ключа Lk, полученного от B
        A.K();
        A.encrypt();                             // A шифрует текст шифром Цезаря со сдвигом K mod 26
        B.decrypt();
    }



}
