package edu.my;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Alex on 23.11.2016.
 */
public class Sender extends User {

    // Передающая сторона генерирует числа a, n

    public int generate_a() {
        a = new Random().nextInt(11);
        return a;
    }

    public BigInteger generate_n() {
        n = new BigInteger(1024, new Random());
        return n;
    }

    public void encrypt() {
        Coder coder = new Coder();
        int offset = k.mod(BigInteger.valueOf(26)).intValue();
        coder.getChangedAlphabet(offset);
        coder.encrypt("Test.txt", "Container.txt");
    }

}
