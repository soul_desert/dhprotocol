package edu.my;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by Alex on 10.11.2016.
 */
public class Coder {

    private List<String> originalAlphabet;
    private List<String> Alphabet;

    public Coder() {
        String abc[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
                "t", "u", "v", "w", "x", "y", "z"};
        originalAlphabet = new ArrayList<>(Arrays.asList(abc));
        Alphabet = new ArrayList<>(Arrays.asList(abc));
    }

    public void getChangedAlphabet(int offset) {
        for (int i = 0; i < offset; i++) {
            List<String> ChangedAlphabet = new ArrayList<>();
            String lastElement = Alphabet.get(Alphabet.size() - 1);
            ChangedAlphabet.add(0, lastElement);
            for (int j = 1; j < Alphabet.size(); j++) {
                ChangedAlphabet.add(j, Alphabet.get(j-1));
                System.out.println(Alphabet.get(j) + " -> " + ChangedAlphabet.get(j));
            }
            Alphabet = ChangedAlphabet;
        }

    }

    public void encrypt(String fileName, String whereTo) {
        try (FileReader fr = new FileReader(fileName)) {
            FileWriter fw = new FileWriter(whereTo);
            int c;
            while((c = fr.read()) != -1) {
                char encryptedSymbol = this.encryptSymbol((char)c);
                // Записать:
                try {
                    fw.write(encryptedSymbol);
                } catch (IOException e) {
                    System.out.println("I/O Error: " + e);
                }
            }

            fw.flush();

        } catch (IOException e) {
            System.out.println("I/O Error: " + e);
        }
    }

    private char encryptSymbol(char symbol) {
        if (originalAlphabet.contains(Character.toString(symbol).toLowerCase())) {
            String letter = Character.toString(symbol);
            if (Objects.equals(letter, letter.toLowerCase())) {
                // если строчная буква
                return Alphabet.get(originalAlphabet.indexOf(letter)).charAt(0);
            } else {
                // если прописная буква
                return Alphabet.get(originalAlphabet.indexOf(letter.toLowerCase())).toUpperCase().charAt(0);
            }

        }
        return symbol; // что угодно кроме букв латинского алфавита
    }

}
